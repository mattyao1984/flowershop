FlowerShop::Application.routes.draw do
  root to: "static_pages#home"

  resources :static_pages

  post "/checkout", to: "static_pages#create"
end
