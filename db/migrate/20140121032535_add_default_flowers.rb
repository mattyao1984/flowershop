class AddDefaultFlowers < ActiveRecord::Migration
  def change
  	flowers = [{
  			name: "Roses",
  			code: "R12",
  			image: "rose.jpeg"
  		},{
  			name: "Lilies",
  			code: "R09",
  			image: "lilies.png"
  		},{
  			name: "Tulips",
  			code: "T58",
  			image: "tulip.png"
  		}
  	]
  	flowers.each do |f|
  		Flower.create(f)
  	end
  end
end