class CreateFlowers < ActiveRecord::Migration
  def change
    create_table :flowers do |t|
      t.text :name
      t.text :code

      t.timestamps
    end
  end
end
