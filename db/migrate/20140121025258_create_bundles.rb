class CreateBundles < ActiveRecord::Migration
  def change
    create_table :bundles do |t|
      t.integer :flower_id
      t.integer :b_num
      t.float :b_amount

      t.timestamps
    end
  end
end
