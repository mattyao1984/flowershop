class AddDefaultBundles < ActiveRecord::Migration
  def change
  	bundles = [{
  			flower_id: 1,
  			b_num: 5,
  			b_amount: 6.99
  		},
  		{
  			flower_id: 1,
  			b_num: 10,
  			b_amount: 12.99
  		},
  		{
  			flower_id: 2,
  			b_num: 3,
  			b_amount: 9.95
  		},
  		{
  			flower_id: 2,
  			b_num: 6,
  			b_amount: 16.95
  		},
  		{
  			flower_id: 2,
  			b_num: 9,
  			b_amount: 24.95
  		},
  		{
  			flower_id: 3,
  			b_num: 3,
  			b_amount: 5.95
  		},
  		{
  			flower_id: 3,
  			b_num: 5,
  			b_amount: 9.95
  		},
  		{
  			flower_id: 3,
  			b_num: 9,
  			b_amount: 16.99
  		}
  	]
  	bundles.each do |b|
  		Bundle.create(b)
  	end
  end
end