class AddPriceToFlower < ActiveRecord::Migration
  def change
  	add_column :flowers, :price, :decimal

  	execute("UPDATE flowers SET price = 1.85 WHERE name = 'Roses'")
  	execute("UPDATE flowers SET price = 3.25 WHERE name = 'Lilies'")
  	execute("UPDATE flowers SET price = 2.75 WHERE name = 'Tulips'") 
  end
end
