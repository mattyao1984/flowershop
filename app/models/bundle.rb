class Bundle < ActiveRecord::Base
	belongs_to :flower
	default_scope :order => "b_amount DESC"
end
